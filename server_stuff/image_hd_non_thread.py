import io
import socket
import struct
import os
import shutil
import time
from PIL import Image
from imutils import paths
import cv2
#import darknet
import re
import datetime
import thread
import darknet

folder = '/home/engineer/CNN_explore/darknet/frames'
folder_path = folder
DestDir = '/home/engineer/CNN_explore/darknet/resize_frames'

# Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
# all interfaces)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('0.0.0.0', 8000))
server_socket.listen(0)

#net = darknet.load_net("cfg/yolo.cfg", "yolo.weights", 0)
#meta = darknet.load_meta("cfg/coco.data")

net = darknet.load_net("cfg/yolov3.cfg", "yolov3.weights", 0)
meta = darknet.load_meta("cfg/coco.data")
#./darknet classifier predict cfg/imagenet1k.data cfg/extraction.cfg extraction.weights data/eagle.jpg
#net = darknet.load_net("cfg/yolo.cfg", "yolo.weights", 0)
#meta = darknet.load_meta("cfg/coco.data")
#./darknet detect cfg/yolov3.cfg yolov3.weights

fm_dict = {}

# Accept a single connection and make a file-like object out of it
connection = server_socket.accept()[0].makefile('rb')
count = 0
# threads related stuff
num_threads = 0
lock = thread.allocate_lock()

def variance_of_laplacian(image):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        return cv2.Laplacian(image, cv2.CV_64F).var()
        #return cv2.Laplacian(image, cv2.CV_64FC3).var()	// for computing laplacian on color images, but takes time

def pick_best_frame():
        global last_key
        start_time = time.time()
        # for imagePath in paths.list_images('./frames/'):
	#print 'picking best frames from ', DestDir
        #for imagePath in paths.list_images(DestDir):
	print 'picking best frames from ', folder
        for imagePath in paths.list_images(folder):
                image = cv2.imread(imagePath)
                # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		re_size_img = cv2.resize(image, (640, 480), interpolation=cv2.INTER_CUBIC)	
                gray = cv2.cvtColor(re_size_img, cv2.COLOR_BGR2GRAY)
                fm = variance_of_laplacian(gray)
                #fm = variance_of_laplacian(gray)
                #fm = variance_of_laplacian(image)
                text = "Not Blurry"
                path =  os.path.basename(imagePath)
                fm_loc = {fm : path}
                fm_dict.update(fm_loc)
        key_list = fm_dict.keys()
        last_key = sorted(fm_dict.keys())[-1]
        for key, value in fm_dict.iteritems():
                print 'fm:' , value, 'key:' , key
        print fm_dict.get(last_key)
        ret_fm = fm_dict.get(last_key)
        print 'Time to pick best-frame: ', (time.time() - start_time)
	#return True
        if last_key > 80.0:
                return True
        else:
                return False

def remove_img_dump():
        start_time = time.time()
        print 'Flushing Image-dump...'
        shutil.rmtree('./frames',ignore_errors=True)
        # os.remove('./videos/demo_gucview.avi')
        os.mkdir('./frames')
	shutil.rmtree('./resize_frames',ignore_errors=True)
	os.mkdir('./resize_frames')
        print 'Time to Flush-dump: ', (time.time() - start_time)

while True:
	print 'New Iteration'
	best_cnt = 0
	count = 0
	while True:	# Loop to capture Images
		# Read the length of the image as a 32-bit unsigned int. If the
		# length is zero, quit the loop
		if count == 1:
			start_time = time.time()
		data = connection.read(struct.calcsize('<L'))
		#print 'data-recv:', data
		#if data == '1122':
		#    print 'I am Done, Kicking out' 
		#    break
			
		image_len = struct.unpack('<L', data)[0] #connection.read(struct.calcsize('<L')))[0]
		#print "img_len = ",image_len
		if not image_len:
		    print 'I am Done, Kicking out' 
		    break
		# Construct a stream to hold the image data and read the image
		# data from the connection
		image_stream = io.BytesIO()
		image_stream.write(connection.read(image_len))
		# Rewind the stream, open it as an image with PIL and do some
		# processing on it
		image_stream.seek(0)
		image = Image.open(image_stream)
		#image.save("./frames/image" + str(count) + ".raw")
		file_name = "./frames/image" + str(count) + ".jpeg"
		image.save(file_name)
		#print('Image is %dx%d' % image.size)
		image.verify()
		# thread.start_new_thread( resize_thread, ("Thread-", file_name, ) )
		count += 1
	print "Received images: ", (count + 1)
	if pick_best_frame():
                    print 'Process Image to ML'         # Process the frame to ML
                    print fm_dict.get(last_key)
                    img_name = fm_dict.get(last_key)
                    print img_name
		    darknet_path = './frames/'+ img_name
		    
                    # Pushing the best image for detection to darknet 
		    start = time.time() 
		    res = darknet.detect(net, meta, darknet_path)
		    print res,"time taken for darkenet detection :",(time.time()-start)
                    #send the text response back to the client(rpi)
		    res = 'done'
		    connection.write(res)
		    print 'total time : ', (time.time() - start_time)
		    res = []	
		    # Creating a local copy of Best Image
		    ext = re.findall(r'.[A-Za-z]+', img_name)
		    new_file = "./test_imageset_lab/" + datetime.datetime.now().strftime("%d_%m_%Y_%H:%M:%S") + ext[1] 
		    # print "ext: ", ext, "[0]:", ext[0],"[1]", ext[1]
		    cp_cmd = 'cp' + ' ' + darknet_path + ' ' + new_file 
		    os.system(cp_cmd)
                    img_name = ''
                    last_key = ''
                    fm_dict.clear()
	else:
                    print 'Poor Image Quality, Try Again !'                     # Ask User For Another Image
		    connection.write('Bad Image Try again')
                    fm_dict.clear()
	
	connection.flush()
	#remove_img_dump()

#finally:
print "In finally closing the socket"
connection.close()
server_socket.close()
