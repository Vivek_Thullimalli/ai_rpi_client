import io
import socket
import struct
import os
import shutil
import time
from PIL import Image
from imutils import paths
import cv2
import darknet
import re
import datetime

# Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
# all interfaces)
print 'socket creation'
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print "socket created"
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('0.0.0.0', 8000))
print 'binded to port'
server_socket.listen(0)
print 'After listen'
#net = darknet.load_net("cfg/yolo.cfg", "yolo.weights", 0)
#meta = darknet.load_meta("cfg/coco.data")

net_detect = darknet.load_net("cfg/yolov3.cfg", "yolov3.weights", 0)
meta_detect = darknet.load_meta("cfg/coco.data")

net_classify = darknet.load_net("cfg/vgg-16.cfg", "weights/vgg-16.weights", 0)
meta_classify = darknet.load_meta("cfg/imagenet1k.data")

#./darknet classifier predict cfg/imagenet1k.data cfg/extraction.cfg extraction.weights data/eagle.jpg
#net = darknet.load_net("cfg/yolo.cfg", "yolo.weights", 0)
#meta = darknet.load_meta("cfg/coco.data")
#./darknet detect cfg/yolov3.cfg yolov3.weights

fm_dict = {}

# Accept a single connection and make a file-like object out of it
connection = server_socket.accept()[0].makefile('rb')
count = 0
print 'acceted the request '

def dict_find(list1):
        label_list = ('bag','book','bottle','chair','cpu','cro','drawer','fireextinguisher','helmet','keyboard','mobile','monitor','pen','printer','router','speaker','spectacles','switchboard','watch','webcam')
        print list1
        dict1 = {'bag':['backpack', 'sleeping bag', 'mailbag',  'punching bag'],'book':['notebook', 'rule', 'comic book'],'bottle':['pop bottle', 'cocktail shaker', 'water bottle', 'beaker', 'beer bottle', 'soap dispenser'],'chair':['barber chair', 'rocking chair', 'folding chair' ],'cpu':['desktop computer'],'cro':['oscilloscope'],'drawer':['desk', 'wardrobe', 'chiffonier', 'bookcase','medicine chest'],'fireextinguisher':['gas pump'],'helmet':['crash helmet', 'football_helmet'],'keyboard':['typewriter keyboard', 'computer keyboard', 'space bar'],'mobile':['cellular telephone','pay-phone'],'monitor':['desktop computer', 'screen', 'monitor'],'pen':['ballpoint', 'fountain pen'],'printer':['photocopier', 'printer'],'router':['modem'],'speaker':['loudspeaker','cassette player', 'tape player', 'radio', 'home theater'],'spectacles':['sunglasses', 'sunglass', 'loupe'],'switchboard':['switch'],'watch':['digital clock', 'wall clock', 'digital watch'],'webcam':['reflex_camera', 'paranoid camera', 'Polaroid camera']}
        l = []
        ind=[]
        for y in label_list:
                #print "Lables : ",y
                for x in list1:
                        #print"List : ",x
                        if x in dict1[y]:
                                ind.append(list1.index(x))
                                l.append(y)
                                #print l,
                                #print y
                                #return 'bag'
                        else:
                                #print x
                                pass
                                #l.append(x)
                                #return x
        #return l,ind
        lent=set(range(len(list1)))
        #print lent
        ind=set(ind)
        #print ind
        diff=list(lent.difference(ind))
        #print diff
        '''for j in range(len(diff)):
                #print "appaending",j
                l.append(list1[diff[j]])
                #print list1[diff[j]]'''

        return l


def variance_of_laplacian(image):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        return cv2.Laplacian(image, cv2.CV_64F).var()

def pick_best_frame():
        global last_key
        start_time = time.time()
        for imagePath in paths.list_images('./frames/'):
                image = cv2.imread(imagePath)
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                fm = variance_of_laplacian(gray)
                text = "Not Blurry"
                path =  os.path.basename(imagePath)
                fm_loc = {fm : path}
                fm_dict.update(fm_loc)
        key_list = fm_dict.keys()
        last_key = sorted(fm_dict.keys())[-1]
        #for key, value in fm_dict.iteritems():
         #       print 'fm:' , value, 'key:' , key
        print fm_dict.get(last_key)
        ret_fm = fm_dict.get(last_key)
        print 'Time to pick best-frame: ', (time.time() - start_time)
	#return True
        if last_key > 80.0:
                return True
        else:
                return False

def remove_img_dump():
        start_time = time.time()
        print 'Flushing Image-dump...'
        shutil.rmtree('./frames',ignore_errors=True)
        # os.remove('./videos/demo_gucview.avi')
        os.mkdir('./frames')
        print 'Time to Flush-dump: ', (time.time() - start_time)

while True:
	print 'New Iteration'
	best_cnt = 0
	count = 0
	while True:	# Loop to capture Images
		# Read the length of the image as a 32-bit unsigned int. If the
		# length is zero, quit the loop
		data = connection.read(struct.calcsize('<L'))
		#print 'data-recv:', data
		#if data == '1122':
		#    print 'I am Done, Kicking out' 
		#    break
			
		image_len = struct.unpack('<L', data)[0] #connection.read(struct.calcsize('<L')))[0]
		#print "img_len = ",image_len
		if not image_len:
		    print 'I am Done, Kicking out' 
		    break
		# Construct a stream to hold the image data and read the image
		# data from the connection
		image_stream = io.BytesIO()
		image_stream.write(connection.read(image_len))
		# Rewind the stream, open it as an image with PIL and do some
		# processing on it
		image_stream.seek(0)
		image = Image.open(image_stream)
		image.save("./frames/image" + str(count) + ".jpeg")
		#print('Image is %dx%d' % image.size)
		image.verify()
		#print('Image is verified')
		count += 1
	print "Received images: ", (count + 1)
	if pick_best_frame():
                    print 'Process Image to ML'         # Process the frame to ML
                    print fm_dict.get(last_key)
                    img_name = fm_dict.get(last_key)
                    print img_name
		    darknet_path = "./frames/" + img_name
		    
                    # Pushing the best image for detection to darknet 
		    start = time.time()

		    # Detector starting 
		    res_detector = darknet.detect(net_detect, meta_detect, darknet_path)
		    print 'detector output - ', res_detector,"time taken for darkenet detection :",(time.time()-start)
		    # Detector End

		    # Classifier Starting
		    res_classifier = darknet.classify(net_classify, meta_classify, darknet_path)
                    print 'classifier output - ', res_classifier[:5]
                    #for i in range(5):
		    post_classify = dict_find(res_classifier[:5])
		    final_res = []
		    for i in res_detector:
		    	if i in ['person', 'keyboard', 'mouse']:
				final_res.append(i)
		    final_res.extend(post_classify)
		    print 'final list to send..',final_res
                    #send the text response back to the client(rpi)
		    connection.write(final_res)
		    res = []	
		    # Creating a local copy of Best Image
		    ext = re.findall(r'.[A-Za-z]+', img_name)
		    new_file = "./best_frames/" + datetime.datetime.now().strftime("%d_%m_%Y_%H:%M:%S") + ext[1] 
		    # print "ext: ", ext, "[0]:", ext[0],"[1]", ext[1]
		    cp_cmd = 'cp' + ' ' + darknet_path + ' ' + new_file 
		    os.system(cp_cmd)
                    img_name = ''
                    last_key = ''
                    fm_dict.clear()
	else:
                    print 'Poor Image Quality, Try Again !'                     # Ask User For Another Image
		    connection.write('Bad Image Try again')
                    fm_dict.clear()

	connection.flush()
	# remove_img_dump()

#finally:
print "In finally closing the socket"
connection.close()
server_socket.close()
