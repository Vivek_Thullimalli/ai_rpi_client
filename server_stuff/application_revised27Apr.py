import io
import socket
import struct
import os
import shutil
import time
from PIL import Image
from imutils import paths
import cv2
import darknet 
import re
import datetime
import sys, os
import pandas as pd
import pdb
import csv
#sys.path.append(os.path.join(os.getcwd(),'python/'))
#darknet.set_gpu(0)
#darknet_path="/home/engineer/CNN_explore/darknet/test_imageset_lab/06_04_2018_12:26:14.jpeg"
'''imgFile = cv2.imread(darknet_path)

cv2.imshow('dst_rt', imgFile)
cv2.waitKey(0)'''

COND = 3
cfg_file_lst = ["cfg/darknet.cfg", "cfg/darknet19_448.cfg", "cfg/darknet19.cfg", "cfg/densenet201.cfg", "cfg/resnet50.cfg", "cfg/resnet152.cfg", "cfg/vgg-16.cfg"]
wt_file_list = ["weights/darknet.weights", "weights/darknet19_448.weights", "weights/darknet19.weights", 
		"weights/densenet201.weights", "weights/resnet50.weights", "weights/resnet152.weights", "weights/vgg-16.weights"]

net = darknet.load_net(cfg_file_lst[COND], wt_file_list[COND], 0)
meta = darknet.load_meta("cfg/imagenet1k.data")

#net = darknet.load_net("cfg/darknet19_448.cfg", "weights/darknet19_448.weights", 0)
#meta = darknet.load_meta("cfg/imagenet1k.data")
#meta = darknet.load_meta("cfg/imagenet22k.dataset")

#csvfile = open('result.csv', 'wb')
#writer = csv.writer(csvfile, delimiter=',')#lineterminator='\n')#, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
#out = open('result.csv', 'w')
#folder = '/home/engineer/Desktop/venkat/updated_test_imageset/'
#folder = '/home/engineer/updated_test_imageset/'
#folder = '/home/engineer/CNN_explore/darknet/outside_img_dump/all_640x480/'
#folder = '/home/engineer/CNN_explore/darknet/outside_analysis/All_actual/'
#folder = '/home/engineer/CNN_explore/darknet/outside_analysis/All_set1_1280x720/'
folder = '/home/engineer/CNN_explore/darknet/outside_analysis/All_set1_640x480/'
#folder = '/home/engineer/CNN_explore/darknet/outside_analysis/All_set1_224x224/'

#df = pd.DataFrame(columns=['fname', 'iname', 'res'])
#raw_data = {'fname': [],
#        'iname': []
#	'res': []}
#df = pd.DataFrame(raw_data, columns = ['fname', 'iname', 'res'])

fname, iname, res2 = [], [], []

z=0

def string_replace(list1):
	list_bag = ['backpack', 'sleeping bag', 'punching bag', 'mailbag']
	if list1[0] in list_bag:
		return 'bag'

def dict_find(list1):
        label_list = ('bag','book','bottle','chair','cpu','oscilloscope','drawer','fireextinguisher','helmet','keyboard','mobile','monitor','pen','printer','router','speaker','spectacles','switchboard','watch','webcam', 'bus', 'truck', 'car', 'sidewalk', 'streetsign', 'roadway', 'bike', 'trafficlight', 'pedestriancrossing', 'bicycle')
        #print "list1",list1
        dict1 = {'bag':['backpack', 'sleeping bag', 'mailbag',  'punching bag'],'book':['notebook', 'rule', 'comic book'],'bottle':['pop bottle', 'cocktail shaker', 'water bottle', 'beaker', 'beer bottle', 'soap dispenser'],'chair':['barber chair', 'rocking chair', 'folding chair' ],'cpu':['desktop computer'],'oscilloscope':['oscilloscope'],'drawer':['desk', 'wardrobe', 'chiffonier', 'bookcase','medicine chest'],'fireextinguisher':['gas pump'],'helmet':['crash helmet', 'football_helmet'],'keyboard':['typewriter keyboard', 'computer keyboard', 'space bar'],'mobile':['cellular telephone','pay-phone'],'monitor':['desktop computer', 'screen', 'monitor'],'pen':['ballpoint', 'fountain pen'],'printer':['photocopier', 'printer'],'router':['modem'],'speaker':['loudspeaker','cassette player', 'tape player', 'radio', 'home theater'],'spectacles':['sunglasses', 'sunglass', 'loupe'],'switchboard':['switch'],'watch':['digital clock', 'wall clock', 'digital watch'],'webcam':['reflex_camera', 'paranoid camera', 'Polaroid camera'],'bus':['trolleybus', 'minibus', 'school bus', 'shuttle bus', 'bus'], 'truck':['delivery truck', 'dump truck', 'laundry truck', 'ladder truck', 'truck', 'garbage truck', 'tow truck', 'trailer truck', 'fire engine'], 'car':['cabin car', 'pace car', 'armored car', 'limousine', 'cab', 'jeep', 'Model T', 'sports car', 'streetcar'], 'sidewalk':['sidewalk'], 'streetsign':['streetsign'], 'roadway':['roadway', 'road', 'arterial road', 'superhighway', 'divided highway', 'driveway', 'runway', 'byway'], 'bike':['mountain bike', 'min bike', 'motor scooter', 'twowheeler'], 'trafficlight':['trafficlight','signaling device'], 'pedestriancrossing':['pedestriancrossing'], 'bicycle':['bicycle', 'push-bike', 'trail bike', 'bicycle-built-for-two']}
        l = []
        ind=[]
        for y in label_list:
                #print "Lables : ",y
                for x in list1:
                        #print"List : ",x
                        if x in dict1[y]:
                                ind.append(list1.index(x))
                                l.append(y)
                                #print l,
                                #print y
                                #return 'bag'
                        else:
                                #print x
                                pass
                                #l.append(x)
                                #return x
        #return l,ind
        lent=set(range(len(list1)))
        #print lent
        ind=set(ind)
        #print ind
        diff=list(lent.difference(ind))
        #print diff
        for j in range(len(diff)):
                #print "appaending",j
                l.append(list1[diff[j]])
                #print list1[diff[j]]

        return l


# Below code runs on folders of folders

for i in os.listdir(folder):
	#print i
	#os.chdir('/home/engineer/Desktop/venkat/test_imageset_lab/'+ i)
	for j in os.listdir(folder+ i):
		#print j
		path = folder + i + '/' + j
		res = darknet.classify(net, meta, path)
		#print j, res[:5]
	
		print "\n", j, "darknet identified obj:", len(res), "Objects: ", res[:5]
		res4 = res[:5]
		#for i in range(5):
		fname.append(i), iname.append(j), res2.append(dict_find(res4))
		#string1 = ''.join(i) + ',' + ''.join(j) + ',' + ''.join(res[:2])
		#df = df.append({'fname': i}, {'iname': j}, {'res': res[:5]})
		#print list(string1)
        	#df.loc[z] = [list(string1)]  #["pen","asd","fff"]]#random.randint(-1,1) for n in range(3)]
		#z += 1
		#print(df)
		#df.to_csv('new.csv', index=False)
'''
# run the code on single folder , 
# THis chage was made because the i
# outside labset all images were clubbed to single folder
for i in os.listdir(folder):
        #print i
        #os.chdir('/home/engineer/Desktop/venkat/test_imageset_lab/'+ i)
        #for j in os.listdir(folder+ i):
	#print j
	path = folder + i
	res = darknet.classify(net, meta, path)
	print i, res[:5]
	res4 = res[:5]
	#for i in range(5):
	#fname.append(i), iname.append(j), res2.append(dict_find(res4))
	fname.append(i), res2.append(dict_find(res4))
'''
print res2
#raw_data = {'fname1': fname, 'iname1': iname, 'res1': res}
df = pd.DataFrame(columns=['fname1', 'iname1', 'res1'])
df['fname1'] = fname
df['iname1'] = iname
df['res1'] = res2
print df
#df.to_csv('darknet19_448.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)

if COND == 0:
	df.to_csv('darknet.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)
elif COND == 1:
	df.to_csv('darknet19_448.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)
elif COND == 2:
	df.to_csv('darknet19.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)
elif COND == 3:
	df.to_csv('densenet201.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)
elif COND == 4:
	df.to_csv('resnet50.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)
elif COND == 5:
	df.to_csv('resnet152.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)
elif COND == 6:
	df.to_csv('vgg-16.csv', columns=['fname1', 'iname1', 'res1'], sep=',', index=False)

 
