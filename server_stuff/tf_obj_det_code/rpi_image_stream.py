from models import object_detection
from config import config
import cv2
import tensorflow as tf
import time
import socket
import struct
import os
import shutil
import time
from PIL import Image
from imutils import paths
import datetime
import io

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('0.0.0.0', 8000))
server_socket.listen(0)

model_name = config.models["5"]
net = object_detection.Net(graph_fp='%s/frozen_inference_graph.pb' % model_name,
                           labels_fp='data/label.pbtxt',
                           num_classes=100,
                           threshold=0.3)
CAMERA_MODE = 'camera'
STATIC_MODE = 'static'
IMAGE_SIZE = 640

# Accept a single connection and make a file-like object out of it
connection = server_socket.accept()[0].makefile('rb')
count = 0

def rpi_edge_stream(mode=CAMERA_MODE):

	#dummy stream
	img_fp = '/home/engineer/frame_68_.jpg'
	img = cv2.imread(img_fp)
	resize_frame = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))
	net.predict(img=resize_frame, display_img=img)
	while True:
		print 'New Iteration'
		best_cnt = 0
		count = 0
		while True:     # Loop to capture Images
			# Read the length of the image as a 32-bit unsigned int. If the
			# length is zero, quit the loop
			if count == 1:
				start_time = time.time()
			data = connection.read(struct.calcsize('<L'))
			#print 'data-recv:', data
			#if data == '1122':
			#    print 'I am Done, Kicking out' 
			#    break

			image_len = struct.unpack('<L', data)[0] #connection.read(struct.calcsize('<L')))[0]
			#print "img_len = ",image_len
			if not image_len:
			    print 'I am Done, Kicking out'
			    break
			image_stream = io.BytesIO()
			image_stream.write(connection.read(image_len))
			image_stream.seek(0)
			img = Image.open(image_stream)
			img.save('dummy.jpeg')
			img.verify()
			img = cv2.imread('dummy.jpeg')	
			in_progress = net.get_status()
			if (not in_progress):
			    resize_frame = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))
			    net.predict(img=resize_frame, display_img=img)
			else:
			    print('[Warning] drop frame or in progress')
			if cv2.waitKey(1) & 0xFF == ord('q'):
			    break	
			# resize_frame = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))
			# net.predict(img=resize_frame, display_img=img)	
	

if __name__ == '__main__':
	#demo(mode=STATIC_MODE)
	#demo(mode=CAMERA_MODE)
	rpi_edge_stream(mode=CAMERA_MODE)

