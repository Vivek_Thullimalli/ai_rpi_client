import numpy as np
import cv2
import time
from imutils import paths
import imutils
import argparse
import os
import glob
import shutil
#import fileSender
import pyttsx3
from imutils.video import FileVideoStream
from imutils.video import FPS
import subprocess
from picamera import PiCamera
from time import sleep
import readchar
'''import rpi_client '''


#engine = pyttsx3.init()
#fileSender.open_socket()
#last_key = ''
fm_list = []
fm_dict = {}

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images", required=True,
        help="path to input directory of images")
ap.add_argument("-t", "--threshold", type=float, default=100.0,
        help="focus measures that fall below this value will be considered 'blurry'")
args = vars(ap.parse_args())

cap = cv2.VideoCapture(0)
# 854x480 
#cap.set(cv2.CAP_PROP_FPS, 10)
#cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1280)
#cap.set(cv2.CAP_PROP_FRAME_WIDTH, 720)
cv2.namedWindow("frame")
# Define the codec and create VideoWriter object
# fourcc = cv2.VideoWriter_fourcc(*'XVID')

'''
#opens a global Socket
def open_socket():
        print "In open_socket"
        global com_socket, conn, addr, engine
        com_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        com_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        com_socket.bind((HOST,PORT))
        com_socket.listen(1)
        conn, addr = com_socket.accept()
        engine = pyttsx3.init()                 #temporary pyttsx
        return 0

def close_socket():
        print "In close_socket"
        conn.sendall('')
        conn.close()
'''
def variance_of_laplacian(image):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        return cv2.Laplacian(image, cv2.CV_64F).var()


#120fps means, cv2.VideoWriter() -
# Irrespective of recording time, it will store the Video with 120frames per Sec.
def capture_video():
	global out 
	fps = 30.0
	'''out = cv2.VideoWriter('output.avi',fourcc, fps, (640,480))'''		# fps- is related to Video-file, Not Cam recording (MICROSOFT- WEBCAM)
	#out = cv2.VideoWriter('output.avi',fourcc, fps, (1024,576))		# (Use this for LOGITECH - WEBCAM)
	print 'Cam properties:'
	'''print 'fps   : ', cap.get(CV_CAP_PROP_FPS)
	print 'height: ', cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
	print 'width : ', cap.get(cv2.CAP_PROP_FRAME_WIDTH)'''
	start_time = time.time()
	time_t = fps *1
	count = 0
	# while((time.time() - start_time) < 1):
	while time_t:
		ret, frame = cap.read()						# Microsoft WebCam cant process more than 12fps
		#print 'Recording...'
		time_t -= 1
		if ret==True:
			# out.write(frame)
			# cv2.imshow('frame',frame)
			cv2.imwrite("./video_frames/frame%d.jpg" % count, frame)     # save frame as JPEG file
			count += 1
		else:
			break
	print 'Capture time: ', (time.time() - start_time)
'''
def extracting_frames():
	start_time = time.time()
	vidcap = cv2.VideoCapture('output.avi')
	success,image = vidcap.read()
	count = 0
	success = True
	while success:          # Extracting frames from Video
		cv2.imwrite("./video_frames/out-%d.raw" % count, image)     # save frame as JPEG file
		success,image = vidcap.read()
		print 'Read a new frame: ', success
		count += 1
	print 'Frame Extraction time: ', (time.time() - start_time)
'''

def extracting_frames():
	print("[INFO] starting video file thread...")
	#fvs = FileVideoStream('guvcview_video-1.mkv').start()
	#fvs = FileVideoStream('/home/pi/opencv/test_app/videos/demo1-4.mp4').start()
	fvs = FileVideoStream('/home/pi/opencv/test_app/videos/video.h264').start()
	time.sleep(1.0)
	start_time = time.time()
	# start the FPS timer
	fps = FPS().start()
	count = 0
	# loop over frames from the video file stream
	while fvs.more():
		# grab the frame from the threaded video file stream, resize
		# it, and convert it to grayscale (while still retaining 3
		# channels)
		frame = fvs.read()
		# frame = imutils.resize(frame, width=450)
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		frame = np.dstack([frame, frame, frame])

		# display the size of the queue on the frame
		cv2.putText(frame, "Queue Size: {}".format(fvs.Q.qsize()),
                (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

		# show the frame and update the FPS counter
		cv2.imshow("Frame", frame)
		cv2.imwrite("./video_frames/frame%d.png" % count, frame)
		count += 1
		cv2.waitKey(1)
		fps.update()
	
	# stop the timer and display FPS information
	fps.stop()
	print 'Frame Extraction time: ', (time.time() - start_time)
	print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
	print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))



def pick_best_frame():
	global last_key
	start_time = time.time()
	for imagePath in paths.list_images(args["images"]):
		image = cv2.imread(imagePath)
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		fm = variance_of_laplacian(gray)
		text = "Not Blurry"
		path =  os.path.basename(imagePath)
		fm_loc = {fm : path}
		fm_dict.update(fm_loc)

	key_list = fm_dict.keys()
	last_key = sorted(fm_dict.keys())[-1]
	for key, value in fm_dict.iteritems():
		print 'fm:' , value, '  key:' , key
	print fm_dict.get(last_key)
	ret_fm = fm_dict.get(last_key)
	print 'Time to pick best-frame: ', (time.time() - start_time)
	if last_key > 80.0:
		return True
	else: 
		return False

def remove_img_dump():
	start_time = time.time()
	print 'Flushing prev-dump...'
	shutil.rmtree('/home/engineer/Desktop/Ai/programs/video_frames',ignore_errors=True)
	os.remove('output.avi')
	os.mkdir('/home/engineer/Desktop/Ai/programs/video_frames')
	print 'Time to Flush-dump: ', (time.time() - start_time)
def capture_vid_rpi_cam_module():
        print 'capturing video from RPI-Cam Module'
        start_time = time.time()
        call(["raspivid", "-t", "1000", "-w", "640", "-h",  "480", "-fps", "25", "-o", "pivideo.h264"])
        ''' Records video for 1Sec@25fps: O/P: raw video'''
        # Wrap the raw video with an MP4 container: (Not Req, Extraction possible for RAW Stream also)
        call(["MP4Box", "-add", "pivideo.h264", "pivideo.mp4"])

def capture_using_pipython():
	camera.start_recording('/home/pi/opencv/test_app/videos/video.h264')
	sleep(2)
	camera.stop_recording()

def trigger_cam_pipython():
	global camera
	camera = PiCamera()
	camera.resolution = (640, 480)		# 640*480: possible upto 90fps
	camera.framerate = 50
	camera.start_preview()

'''rpi_client.open_socket()'''
trigger_cam_pipython()
while True:
	# ret, frame = cap.read()
	# cv2.imshow("frame", frame)
	# if not ret:
	#	break
	# k = cv2.waitKey(1)

	#if k%256 == 27:
        # ESC pressed
	#	print("Escape hit, closing...")
	#	break
	#elif k%256 == 32:	# Recording for few seconds
	k = raw_input('Hit space to Capture: ')
	if k == 'e':
		print 'Escape Hit, closing app!'
		break
	elif k == ' ':
		print 'Space Hit'
		'''capture_video()'''
                '''capture_vid_rpi_cam_module()'''
		capture_using_pipython()
		extracting_frames()
		if pick_best_frame():
                    print 'Process Image to ML'		# Process the frame to ML
                    print fm_dict.get(last_key)
                    img_name = fm_dict.get(last_key)
                    print img_name
                    # Pushing the captured imgae to Socket  
                    # fileSender.capture_image("/home/engineer/Desktop/Ai/programs/video_frames/" + img_name)
                    '''print("{} written!".format(img_name))'''
                    '''rpi_client.send_image_via_socket()'''
                    # Waiting for Text Response
                    # fileSender.wait_for_text()
                    img_name = ''
                    last_key = ''
                    fm_dict.clear()
                    #remove_img_dump()
		else:
                    print 'Poor Image Quality, Try Again !'			# Ask User For Another Image
                    data = "Please , can you capture the frames again.."
                    # engine.say(data)
                    # engine.setProperty('rate', 5)
                    # engine.setProperty('volume', 0.9)
                    # engine.runAndWait()

                    fm_dict.clear()
                    #remove_img_dump()
                    #	continue
                    
'''rpi_client.close_socket()'''
#camera.stop_preview()
# Release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()
