import os
import socket
import sys

filepath = '/home/pi/opencv/test_app/video_frames/458italia.jpg'
#filepath = '/home/pi/opencv/test_app/video_frames/out-11.raw'
PORT = 8080
HOST = '192.168.200.139'    # Configure your Server Address here '' 

def open_socket():
	print 'Welcome to the Data Transfer application'
	global socket
	socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	conn_status = socket.connect((HOST, PORT))
	print  'Connection to Server: ', HOST,  conn_status

def close_socket():
	print 'Closing Socket: '
	socket.close()

# 1. Send file Name, Size & IMage
def send_image_via_socket():
	print 'Inimage via socket'
	test_str = ''
	test_str = os.path.basename(filepath)
	print 'test_str: ', test_str
	socket.send(test_str)
	socket.send('\n')		# break by New-line

	#Sending File-size
        size = os.path.getsize(filepath)
	print 'file -size: ', size
        socket.send(str(size))
        socket.send('\n')

	# Send FIle
	fileToSend = open(filepath, 'rb')
	count = 0
        pack_size = 64512   # TCP Packet Size to 64KBytes
	'''while count < size:
		# print 'count : ', count
		byte = fileToSend.read(1)
		socket.send(byte)
		count += 1'''
        while count < size:
                print 'count: ', count
                buff = fileToSend.read(pack_size)
                if buff:
                    socket.send(buff)
                    count += pack_size
                else:
                    break

        print 'count: ', count
	print 'File- Transmission Compleated'

open_socket()
send_image_via_socket()

close_socket()


