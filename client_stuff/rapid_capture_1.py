import io
import socket
import struct
import time
import picamera
import RPi.GPIO as GPIO
import os

def wait_for_text():
        print 'Waiting for Text Respose...'
        #   fname = open('text.txt', 'w')
        data = client_socket.recv(1000)
	print 'Data: ', data
        os.system("espeak ' " + data + " ' ")

class SplitFrames(object):
    def __init__(self, connection):
        self.connection = connection
        self.stream = io.BytesIO()
        self.count = 0

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # Start of new frame; send the old one's length
            # then the data
            size = self.stream.tell()
	    # print '\  '
            if size > 0:
                # print 'size_ret: ' ,self.connection.write(struct.pack('<L', size))
		self.connection.write(struct.pack('<L', size))
                self.connection.flush()
                self.stream.seek(0)
                ret_con = self.connection.write(self.stream.read(size))
                self.count += 1
                self.stream.seek(0)
        ret_stream = self.stream.write(buf)

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)

client_socket = socket.socket()
# client_socket.connect(('192.168.200.140', 8000))
client_socket.connect(('192.168.200.189', 8000))
connection = client_socket.makefile('wb')
# try:
output = SplitFrames(connection)
print "Press the Switch"
while True:
	input_state = GPIO.input(17)
        if input_state == False:
            print('I captured Image')
            with picamera.PiCamera(resolution='VGA', framerate=30) as camera:
                time.sleep(0.1)
                start = time.time()
                #print start
                camera.start_recording(output, format='mjpeg')
                camera.wait_recording(1)
                camera.stop_recording()
                print (time.time()-start)
                # Write the terminating 0-length to the connection to let the
                # server know we're done
                # time.sleep(5)
                print 'sending end parity'
		ret = connection.write(struct.pack('<L', 0))
		connection.flush()
                finish = time.time()
            wait_for_text()
#finally:
connection.close()
client_socket.close()
#finish = time.time()
#print('Sent %d images in %d seconds at %.2ffps' % (
#    output.count, finish-start, output.count / (finish-start)))
