import cv2
import gcloud_detect

cam = cv2.VideoCapture(0)

cv2.namedWindow("test")

img_counter = 0

while True:
    ret, frame = cam.read()
    cv2.imshow("test", frame)
    if not ret:
        break
    k = cv2.waitKey(1)

    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        img_name = "out_images/opencv_frame_{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
#	lst = gcloud_detect.detect_labels(img_name)
        #str = ' '.join(lst)
#        gcloud_detect.text_to_speech(str)
        
        print("{} written!".format(img_name))
        img_counter += 1
        
cam.release()

cv2.destroyAllWindows()

