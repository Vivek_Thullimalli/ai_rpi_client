import argparse
import io

from google.cloud import vision
from google.cloud.vision import types

from gtts import gTTS
import os

def text_to_speech(text):
    print("text_to_speech")
    tts = gTTS(text=text, lang='en')
    tts.save('text.mp3')
    str = 'mpg321' + ' ' + 'text.mp3'
    os.system(str)
    

'''def run_speech(file):
    string = 'mpg321' + ' ' + file
    os.system(string)'''


# [START def_detect_labels]
def detect_labels(path):
    print("In detect_labels")
    """Detects labels in the file."""
    client = vision.ImageAnnotatorClient()
    # [START migration_label_detection]
    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    response = client.label_detection(image=image)
    labels = response.label_annotations
    print('Labels:')

    lst = ['Icansee']
    for label in labels:
        lst.append(label.description)
        print(label.description)
    print(lst)
    return lst
# [END migration_label_detection]
# [END def_detect_labels]

# detect_labels('2.png')
