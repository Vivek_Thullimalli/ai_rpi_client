#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <termios.h>

#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>

#define QUEUE_NUM 4

#define CLEAR(x) memset(&(x), 0, sizeof(x))

static void **m_image_buf;
static size_t m_image_size = 0;
static int m_image_width = 0;
static int m_image_height = 0;


struct termios orig_termios;

void reset_terminal_mode()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
    struct termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);

    new_termios.c_lflag &= ~( ICANON | ECHO );
    tcsetattr(STDIN_FILENO, TCSANOW, &new_termios);
}

int kbhit(void)
{
    struct timeval tv;
    fd_set rdfs;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&rdfs);
    FD_SET (STDIN_FILENO, &rdfs);

    select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &rdfs);
}

static char* _un_fourcc(uint32_t fcc)
{
    static char s[5];

    s[0] = fcc & 0xFF;
    s[1] = (fcc >> 8) & 0xFF;
    s[2] = (fcc >> 16) & 0xFF;
    s[3] = (fcc >> 24) & 0xFF;
    s[4] = '\0';

    return s;
}

static int _device_open(int *camfd)
{
    struct v4l2_capability cap;
    struct v4l2_format format;
    char *devname;
    int fd;

    devname = "/dev/video0";
    //fd = open(devname, O_RDWR | O_NONBLOCK);
    fd = open(devname, O_RDWR);
    if (fd == -1) {
        printf("open camera %s", strerror(errno));
        return 1;
    }

    CLEAR(cap);
    if (ioctl(fd, VIDIOC_QUERYCAP, &cap) == -1) {
        printf("ioctl QUERYCAP %s", strerror(errno));
        return 1;
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        printf("camera don't support Video Capture");
        return 1;
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        printf("camera don't support the stream i/o method");
        return 1;
    }

    CLEAR(format);
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_G_FMT, &format) == -1) {
        printf("ioctl G_FMT %s", strerror(errno));
        return 1;
    }

    //format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    //format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_H264;
    format.fmt.pix.width = 640;
    format.fmt.pix.height = 480;
    if (ioctl(fd, VIDIOC_S_FMT, &format) == -1) {
        printf("ioctl S_FMT %s", strerror(errno));
        return 1;
    }

    /*
     * done
     */
    printf("camera driver name %s\n", cap.driver);
    printf("camera name %s\n", cap.card);
    printf("camera bus %s\n", cap.bus_info);
    printf("camera default %zd %zd format: %s size: %zd\n",
           format.fmt.pix.width, format.fmt.pix.height,
           _un_fourcc(format.fmt.pix.pixelformat), format.fmt.pix.sizeimage);

    *camfd = fd;
    m_image_size = format.fmt.pix.sizeimage;
    m_image_width = format.fmt.pix.width;
    m_image_height = format.fmt.pix.height;

    return 0;
}

static int _device_on(int fd)
{
    struct v4l2_requestbuffers reqbuf;
    CLEAR(reqbuf);
    reqbuf.count = QUEUE_NUM;
    reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    reqbuf.memory = V4L2_MEMORY_USERPTR;
    if (ioctl(fd, VIDIOC_REQBUFS, &reqbuf) == -1) {
        printf("camera don't support user point i/o");
        return 1;
    }

    m_image_buf = calloc(QUEUE_NUM, sizeof(void*));
    for (int i = 0; i < QUEUE_NUM; i++) {
        m_image_buf[i] = malloc(m_image_size);
    }

    for (int i = 0; i < QUEUE_NUM; i++) {
        struct v4l2_buffer buf;

        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_USERPTR;
        buf.index = i;
        buf.m.userptr = (unsigned long)m_image_buf[i];
        buf.length = m_image_size;
        if (ioctl(fd, VIDIOC_QBUF, &buf) == -1) {
            printf("device on ioctl QBUF %s", strerror(errno));
            return 1;
        }
    }

    enum v4l2_buf_type type;
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_STREAMON, &type) == -1) {
        printf("ioctl STREAMON %s", strerror(errno));
        return 1;
    }

    return 0;
}

static int _dqbuf(int fd, struct v4l2_buffer *buf, int *index, size_t *size)
{
    buf->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf->memory = V4L2_MEMORY_USERPTR;
    if (ioctl(fd, VIDIOC_DQBUF, buf) == -1) {
        printf("ioctl DQBUF %s", strerror(errno));
        return 1;
    }

    *index = buf->index;
    *size = buf->bytesused;
//	printf("index: %d, bytes-used: %d \n", buf->index, buf->bytesused);

    // printf("buf %d filled\n", i);

    return 0;
}

static int _qbuf(int fd, struct v4l2_buffer *buf)
{
    if (ioctl(fd, VIDIOC_QBUF, buf) == -1) {
        printf("ioctl QBUF %s", strerror(errno));
        return 1;
    }

    return 0;
}

int camera_init(int *camera_fd)
{
    int fd, rv;

    fd = 0;

    rv = _device_open(&fd);
    if (rv) return rv;

    rv = _device_on(fd);
    if (rv) return rv;

    *camera_fd = fd;

    return 0;
}

void camera_destroy(int fd)
{
    enum v4l2_buf_type type;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    ioctl(fd, VIDIOC_STREAMOFF, &type);
    for (int i = 0; i < QUEUE_NUM; i++) free(m_image_buf[i]);
    free(m_image_buf);
    close(fd);
}

int main(int argc, char *argv[])
{
    char *p;
    char c;
    int fd, rv;
    void *data;
    int w, h;
    size_t size;
    clock_t clock_s, clock_e;
    struct timeval tvs, tve;

    set_conio_terminal_mode();

    rv = camera_init(&fd);
    if (rv) {
        printf("\n init camera failure, exit\n");
        return 1;
    } else printf("init camera ok\n");

    printf("image capture demo, press 'c' to capture, 'q' to quit\n");

    int frame_count = 0;
    int index;
    size_t len;
//    while (1) {
	while(frame_count < 15) {
        struct v4l2_buffer buf;

        CLEAR(buf);
        rv = _dqbuf(fd, &buf, &index, &len);
        if (rv) {
            printf("\n dqbuf nok\n");
            return 1;
        }

//        if (kbhit()) {
//            c = getchar();

            printf("\nGot %c %d on frame %d %d %zd\n",
                   c, frame_count, index, m_image_width * m_image_height, len);

//            if (c == 's') {
                char filename[32];
                //snprintf(filename, sizeof(filename), "./video_frames/out-%d.png", frame_count);
                snprintf(filename, sizeof(filename), "./video_frames/out-%d.raw", frame_count);
                FILE *fp = fopen(filename, "w");
                fwrite(m_image_buf[index], len, 1, fp);
                fclose(fp);
//            }

//            if (c == 'q') {
//                break;
//            }
//        }

        rv = _qbuf(fd, &buf);
        if (rv) {
            printf("\n qbuf nok\n");
            return 1;
        }

        frame_count++;
    }

    camera_destroy(fd);

    return 0;
}
