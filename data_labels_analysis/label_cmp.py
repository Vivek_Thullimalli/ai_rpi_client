import pandas as pd
import os
import csv
from collections import Counter

#output_df=pd.read_csv('/home/engineer/CNN_explore/darknet/label_cmp.csv')

#df_r1 = pd.read_csv('/home/engineer/CNN_explore/darknet/data/coco_names.csv')
df_r1 = pd.read_csv('/home/engineer/CNN_explore/darknet/data_labels_analysis/coco_names.csv')
df_sr1 = df_r1.sort_values(["coco_names"], ascending=[1])
#print "df_Sr1: ", df_sr1

df_r2 = pd.read_csv('/home/engineer/CNN_explore/darknet/data_labels_analysis/label_pbtxt.csv')
df_sr2 = df_r2.sort_values(["tf_obj_label"], ascending=[1])

#voc 2007
df_r3 = pd.read_csv('/home/engineer/CNN_explore/darknet/data_labels_analysis/voc_2007_names.csv')
df_sr3 = df_r3.sort_values(["Voc_2007_names"], ascending=[1])
print "df_Sr3: ", df_sr3

#kitti
df_r4 = pd.read_csv('/home/engineer/CNN_explore/darknet/data_labels_analysis/kitti_label_name.csv')
df_sr4 = df_r4.sort_values(["Kitti"], ascending=[1])
print "df_Sr3: ", df_sr4


lst = [df_sr1, df_sr2, df_sr3, df_sr4]
result = pd.concat(lst, axis=1)
#result.to_csv('label_cmp.csv', index=False)
fin_res = result.sort_values(["coco_names", "tf_obj_label", "Voc_2007_names", "Kitti"], ascending=[1,1,1,1])
fin_res.to_csv('label_cmp.csv', index=False)

