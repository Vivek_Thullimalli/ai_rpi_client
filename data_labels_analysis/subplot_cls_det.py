import matplotlib.pyplot as plt
import pandas as pd
import csv

#df=pd.read_csv("/home/engineer/CNN_explore/darknet/net_bargraph_640480.csv")
df=pd.read_csv("/home/engineer/darknet/det_classify_analysis.csv")

#ax = df[['NetType', 'Hits_1920x1080', 'Hits_1280x720', 'Hits_640x480', 'Hits_224x224']].plot(ylim = (0,200),kind = 'bar', rot = 0, grid = True, title = 'demo', width = 0.2, figsize = (8, 5), legend = True, fontsize = 15, color = 'ygbr')
ax = df[['NetType', 'Threshold_0.3_det', 'Threshold_0.3_classifier', 'Threshold_0.4_det','Threshold_0.4_classifier','Threshold_0.5_det', 'Threshold_0.5_classifier']].plot(ylim = (0,0.5), kind = 'line', rot = 0, title = 'Detector: YOLO-9000', figsize = (8, 5), legend = True, fontsize = 15, color = 'ygbrcm',grid = True)
ax.set_xlabel("NetTypes", fontsize = 24)
ax.set_ylabel("Hits", fontsize = 24)
ax.set_ylim = [0, 0.05, 0.1, 0.15, 0.20, 0.25, 0.30]

x_dummy = (0,1,2,3,4,5,6)
x_axis_list = []
for index, row in df.iterrows():
        x_axis_list.append(row['NetType'])

x_axis_tuple = tuple(x_axis_list)
plt.xticks(x_dummy, x_axis_list)









plt.show()
#df.plot(ax)
#df2.plot(ax=axes[0,1])
