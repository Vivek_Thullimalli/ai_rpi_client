import matplotlib.pyplot as plt
import pandas as pd
import csv

#df=pd.read_csv("/home/engineer/CNN_explore/darknet/net_bargraph_640480.csv")
df=pd.read_csv("/home/engineer/CNN_explore/darknet/net_bargraph_out_unlabelled_12.csv")

#ax = df[['NetType', 'Hits_1920x1080', 'Hits_1280x720', 'Hits_640x480', 'Hits_224x224']].plot(ylim = (0,200),kind = 'bar', rot = 0, grid = True, title = 'demo', width = 0.2, figsize = (8, 5), legend = True, fontsize = 15, color = 'ygbr')
ax = df[['NetType', 'Hits_1920x1080', 'Hits_640x480', 'Hits_224x224']].plot(ylim = (0,200),kind = 'bar', rot = 0, grid = True, title = 'demo', width = 0.2, figsize = (8, 5), legend = True, fontsize = 15, color = 'ygb')
ax.set_xlabel("NetTypes", fontsize = 24)
ax.set_ylabel("Hits", fontsize = 24)
ax.set_ylim = [10, 40, 80, 120, 160, 200, 240]
x_dummy = (0,1,2,3,4,5,6)
x_axis_list = []
for index, row in df.iterrows():
        x_axis_list.append(row['NetType'])

x_axis_tuple = tuple(x_axis_list)
plt.xticks(x_dummy, x_axis_list)

'''
ax1 = df[['NetType', 'Time_224x224']].plot(kind = 'line', grid = True, title = 'demo', figsize = (15, 10), legend = True, fontsize = 15)
ax1.set_xlabel("NetTypes", fontsize = 24)
ax1.set_ylabel("Time", fontsize = 24)

plt.xticks(x_dummy, x_axis_list)
'''

ax2 = df[['NetType', 'Accuracy_1920x1080', 'Accuracy_640x480', 'Accuracy_224x224']].plot(ylim =(0, 100), width = 0.2, kind = 'bar', grid = True, title = 'demo', figsize = (15, 10), legend = True, fontsize = 15, rot = 0, color = 'ygb')
ax2.set_xlabel("NetTypes", fontsize = 24)
ax2.set_ylabel("Accuracy", fontsize = 24)

plt.xticks(x_dummy, x_axis_list)


plt.show()
#df.plot(ax)
#df2.plot(ax=axes[0,1])
