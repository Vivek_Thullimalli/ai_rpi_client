import pandas as pd
import os 
import csv
from collections import Counter
#df=pd.read_csv("new3.csv")

read_file_lst = ["darknet.csv", "darknet19_448.csv", "darknet19.csv", "densenet201.csv", "resnet50.csv", "resnet152.csv", "vgg-16.csv"]

COND_FILE_INP = 1
df_r1=pd.read_csv(read_file_lst[COND_FILE_INP])
file_name_replacements = ("'", "[", "]", " ", ",", "_")
str_replacements = ("'", "[", "]", " ", "-", "_")
	
df_sr1=df_r1.sort_values(["fname1","iname1"], ascending=[1,1])
#dfs=df.sort(["fname1","iname1"], ascending=[1,1])
df_sr1.to_csv('outnew.csv', index=False)
df_g1=pd.read_csv("lab_imagese.csv")
df_sg1=df_g1.sort_values(["FolderName","ImageName"], ascending=[1,1])
#dfs=df.sort(["FolderName","ImageName"], ascending=[1,1])
df_sg1.to_csv('Lab_sort.csv',index=False)

lst = [df_sr1, df_sg1]
#df_final = pd.DataFrame()
#result = pd.concat(lst, axis=1)
#result.to_csv('nayeem.csv', index=False)

dataframes = [pd.read_csv(p) for p in ("Lab_sort.csv", "outnew.csv")]
merged_dataframe = pd.concat(dataframes, axis=1)
merged_dataframe.to_csv("merged.csv", index=False)
compare_res = []
compare_cnt = []

def count_handler(df):
	hits = (df['ComparitiveResult'] == 1).sum()
	#print (df['ComparitiveResult'] == 1).sum()
	dim = df.shape	
	print 'shape:', dim, type(dim)
	print 'dim:', dim[0]
	dic1 ="Total - Hits :" + str(hits)
	dic2 ="Total - Tries:" + str(dim[0])
	#dic2 = [["Total - Tries"], [hits]]
	#df['TOTAL'] = df['TOTAL'].empty
	#df["TOTAL"][0] = dic1
	#df["TOTAL"][1] = dic2
	#print df["ComparitiveResult"][200]
	df4 = pd.DataFrame({'TOTAL': [dic1, dic2]})
	frames = [df, df4]
	result = pd.concat(frames, axis=1)	
	return result	
	

def compare(df):
	#reader = csv.reader()
	#print df['ObjectsInTheImage'], df['res1']
	'''df_r = df['res1']
	l2, l3 = [], []
	for i in df['ObjectsInTheImage']:
		l2.append(list(i.split(',')))
	df['test'] = l2
	print df['test']
	
	for i in df_r:
		l = i in l2
		print l
		#l3.append(i)
		#print i
	#print l3
	return df'''
	for index, row in df.iterrows():
		#print (row["ObjectsInTheImage"], row["res1"])
		print '\nNew Row:----->>> Index: ', index
		temp_gnd_list = row["ObjectsInTheImage1"].split(',')
		ml_res1 = row["res1"].split(',')
		count = 0

		#print "My gnd_list: type:", type(gnd_list)," ml_res1:", type(ml_res1)
		#print 'gnd:', gnd_list, 'ml_res:', ml_res1
		#print ml_res1
		#gnd_list = [w.replace(' ', '') for w in gnd_list]
		gnd_list = []
		for w in temp_gnd_list:					# This will clean the ground truth labels if havaing replacements(tuple)
			for r in str_replacements:
				w = w.replace(r, '')
			gnd_list.append(w)
		print 'temp_gnd_list: ', gnd_list
		for i in ml_res1:
			for r in str_replacements:				# Removing Replacement from ml-string
				i = i.replace(r, '')		
			i = i.lower()	
			print "looking for: ", i, "type:", type(i)
			if i in (name.lower() for name in gnd_list):
				print 'Obj FOund:', i
				count = count + 1
			'''print i in gnd_list 
				print "i:", i, "gnd_list: ", gnd_list
			else:
				print "Not found", i, "type:", type(i)'''
		#print 'Total -Object found:', count
		if count:
			#print "My gnd_list: type:", type(gnd_list)," ml_res1:", type(ml_res1)
			#print 'gnd:', gnd_list, 'ml_res:', ml_res1
			loc_list = 1 
			loc_cnt_list = (count / len(gnd_list))
			#print "count:", count, " per:", loc_list
		else:
			loc_list = 0
			loc_cnt_list = 0	
		compare_res.append(loc_list)
		compare_cnt.append(loc_cnt_list)
		gnd_list = []
		ml_res1 = []
	#print "List:", compare_res
	df["ComparitiveResult"] = compare_res
	df["CountResult"] = compare_cnt 
	return df


#df=pd.read_csv(path)
def String_DataFrame(df,path):
	frame=[]
	with open(path, 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			#print row
			l = row
			#l2 = str(l[])
			k = [i.split(',', -1) for i in row]
			#print k[3]
			#print type(k[3])
			j= Counter(k[3])
			#print j

			b={m:j[m] for m in j}
			frame.append(str(b))
	del(frame[0])
	df["Ground_compare"]=frame
	return df


def List_DataFrame(df,path):
	frame=[]
	with open(path, 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			#print row
			l = row
			#l2 = row[6]
			l3 = row[6]
			print l3.find('['), l3.find(']')
			char = ['[', ']', "'", "'"]
			for i in char:
				l3 = l3.replace(i, '')
			#l3 = l3.replace('[', '')
			#print l3 = l3.replace(']', '')
			
			l4 = l3.split(",")
			print l4
			#print list()
			'''l = row
			l=str(l[6])
			#print l
			l=l.split(',')
			#print l
			l2 = str(l)'''
			#k = [i.split(',', -1) for i in row]
			#l=l.split(',')
			if len(l4) == 0:
				b=None
			else:
				j= Counter(l4)
				#print j
				b={m:j[m] for m in j}
			frame.append(str(b))
	del(frame[0])

	df['Model_EV']=frame
	df['Model_EV'] = df['Model_EV'].replace("{'': 1}", "None") 
	return df
#df.to_csv('count.csv')
if __name__ =='__main__':
	path='merged.csv'
	#path = 'vgg16_darknet.csv'
	df=pd.read_csv(path)
	#df1=List_DataFrame(df,path)
	#df1=String_DataFrame(df,path)
	df1 = compare(df)
	df_final = count_handler(df1)
	file_name = read_file_lst[COND_FILE_INP]
	str_fname = str((str(file_name)).split(".csv"))
	for r in file_name_replacements:
		str_fname = str_fname.replace(r, '')
	fname_final = str_fname + "_accuracy_anly.csv"
	print "Output File:",	fname_final
	df_final.to_csv(fname_final)
	os.remove('Lab_sort.csv')
	os.remove('outnew.csv')

